﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(InputTimer))]

//works as expected but has outofrange error

public class ModifiedMove : MonoBehaviour {

    public List<string> sequence;
    public List<string> currentInput;

    private Orientation adjust;
    private InputTimer timer;
    private Hitbox hitbox;
    private Animator animator;
    private KeyBindings keyBindings;
    private PlayAnim playAnim;

    private bool same = true;
    void Start()
    {
        currentInput = new List<string>();

        timer = GetComponent<InputTimer>();
        playAnim = GetComponent<PlayAnim>();

        animator = transform.parent.GetComponent<Animator>();
        keyBindings = transform.parent.GetComponent<KeyBindings>();
        adjust = transform.parent.GetComponent<Orientation>();

        hitbox = GetComponentInChildren<Hitbox>();
    }

    void Reset()
    {
        Debug.Log("Reset");
        currentInput.Clear();
        timer.StopTimer();
        same = true;
    }

    void Update()
    {
        //Debug.Log(gameObject.name + " " +timer.currentTime);
        SendInput();
        //if (currentInput.Count >= sequence.Count)
            CheckMove(currentInput);              
    }

    public void DoMove()
    {
        if (hitbox != null)
            hitbox.SpawnHitbox();
        if (playAnim != null)
            playAnim.Play(gameObject.name);
        Debug.Log(name);
    }

    void SendInput()
    {
        for (int i = 0; i < keyBindings.keys.Count; i++)
        {
            if (Input.GetKeyDown(keyBindings.keys[i].key))
            {
                timer.StartTimer();
                currentInput.Add(adjust.MoveAdjustment(keyBindings.keys[i].name));
                //Debug.Log(keyBindings.keys[i].name);
            }
        }
    }

    public void CheckMove(List<string> input)
    {
        for (int x = 0; x < input.Count; x++)
        {
            if (input[x] != sequence[x])
            {
            Reset();
                return;
            }
             
        }       
        if (!(timer.currentTime <= 0))
        {
            if (input.SequenceEqual(sequence))
            {
                //execute functionalities
                DoMove();
                Reset();
            }
        }
        else
            Reset();
    }
}
