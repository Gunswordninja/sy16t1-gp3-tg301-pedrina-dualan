﻿using UnityEngine;
using System.Collections;


/// <summary>
/// To be attached to empty GameObject
/// </summary>
/// 
public class Hitbox : MonoBehaviour {

    private Transform actor;
    void Start()
    {
        actor = gameObject.transform.parent.transform.parent;
    }
    //function to be called to Move.cs
    public void SpawnHitbox()
    {
        SendMessage("OnSpawn",actor);
        //other functions to be called
    }
}
