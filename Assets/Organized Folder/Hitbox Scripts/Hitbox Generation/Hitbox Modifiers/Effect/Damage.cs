﻿using UnityEngine;
using System.Collections;

public class Damage : MonoBehaviour
{
    public int damage;
    public string targetTag;
    // Use this for initialization
    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("X");
        if (col.CompareTag(targetTag)) 
		{
				col.GetComponent<Animator>().Play("Hurt State");
				col.GetComponent<EnemyStats>().hp -= damage;     
		}
		if (col.CompareTag("Object"))
		{
				Destroy(col.gameObject);
		}
    }
}

