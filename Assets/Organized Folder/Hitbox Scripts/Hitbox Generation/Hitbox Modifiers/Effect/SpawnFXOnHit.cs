﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class SpawnFXOnHit : MonoBehaviour {

	// Use this for initialization
	// Use this for initialization
	public List<string> targetTag = new List<string>();
	public GameObject HitFX;

	void OnTriggerEnter2D(Collider2D col)
	{
		Debug.Log("X");
		for (int i = 0; i < targetTag.Count; i++)
		{
			if (col.CompareTag(targetTag[i])) {
				Instantiate(HitFX,gameObject.transform.position,gameObject.transform.rotation);
			}
		}
	}
}
