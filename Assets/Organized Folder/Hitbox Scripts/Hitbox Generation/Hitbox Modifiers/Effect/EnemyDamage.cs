﻿using UnityEngine;
using System.Collections;

public class EnemyDamage : MonoBehaviour {

    public int damage;
    public string targetTag;
    // Use this for initialization
	void OnTriggerEnter2D(Collider2D col)
	{
		Debug.Log("X");
		if (col.CompareTag(targetTag))
		{
			if (!col.GetComponent<PlayerColliderScript>().IsInvincible)
			{
				col.GetComponent<Animator>().Play("Hurt State");
				//col.GetComponent<PlayerStats>().Health -= damage;
				col.GetComponent<PlayerColliderScript>().TakeDamage(damage);
			}
		}

	}

}
