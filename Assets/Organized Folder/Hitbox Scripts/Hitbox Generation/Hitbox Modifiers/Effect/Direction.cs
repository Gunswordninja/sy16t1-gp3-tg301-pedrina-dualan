﻿using UnityEngine;
using System.Collections;

public class Direction : MonoBehaviour {

   // public float angleRad;
    public float angleX;
    public float angleY;
    public float strength;
    public int Orient; 
    private Rigidbody2D rBody;
    public string targetTag;
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag(targetTag)) {
            Debug.Log("works");
            rBody = col.GetComponent<Rigidbody2D>();
            //angleRad *= (Mathf.PI / 180);
            rBody.AddForce(new Vector2(angleX * Orient, angleY).normalized * strength, ForceMode2D.Impulse);
        }
        
    }
}

//Mathf.Cos(angleRad), Mathf.Sin(angleRad)