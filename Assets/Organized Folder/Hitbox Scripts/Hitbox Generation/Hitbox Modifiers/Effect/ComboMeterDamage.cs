﻿using UnityEngine;
using System.Collections;

public class ComboMeterDamage : MonoBehaviour
{
    public delegate void OnHit();
    public static event OnHit onHit;
    public int damage;
    public string targetTag;
    // Use this for initialization
    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("X");
        if (col.CompareTag(targetTag)) {
            col.GetComponent<Animator>().Play("Hurt State");
            col.GetComponent<EnemyStats>().hp -= damage;
            onHit();
        }   

		if (col.CompareTag("Object"))
		{
			Destroy(col.gameObject);
		}
    }

}