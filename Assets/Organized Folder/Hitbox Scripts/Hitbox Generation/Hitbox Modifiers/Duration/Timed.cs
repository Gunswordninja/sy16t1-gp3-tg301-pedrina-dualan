﻿using UnityEngine;
using System.Collections;

public class Timed : MonoBehaviour {

    public float delay;

    void Start()
    {
        GameObject.Destroy(gameObject, delay);
    }
}
