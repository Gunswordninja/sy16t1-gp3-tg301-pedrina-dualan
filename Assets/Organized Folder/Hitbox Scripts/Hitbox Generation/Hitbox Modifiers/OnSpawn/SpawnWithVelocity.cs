﻿using UnityEngine;
using System.Collections;

public class SpawnWithVelocity : MonoBehaviour {

    //adjust script for it to have proper direction
    public GameObject prefab;
    public float force;

    //fix this
    public void OnSpawn(Vector2 origin)
    {
        GameObject projectile = Instantiate(prefab) as GameObject;
        projectile.transform.position = origin;
        //replace vector3.forward and make it dependent on where actor is facing
        projectile.GetComponent<Rigidbody>().velocity = origin.normalized * force;
    }
}
