﻿using UnityEngine;
using System.Collections;

public class Spawn_StateMVer : StateMachineBehaviour {

    // Use this for initialization
    public Vector2 Size;
    public Vector2 offSet;

    public GameObject prefab;
    public float SpawnTime;
    private float timeElapsed;
    private bool spawn = false;

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timeElapsed += Time.deltaTime;

        if (timeElapsed >= SpawnTime && spawn)
        {
         GameObject projectile = Instantiate(prefab) as GameObject;
        //print(animator.name);
        Physics2D.IgnoreCollision(projectile.GetComponent<Collider2D>(), animator.GetComponent<Collider2D>());
        projectile.GetComponent<BoxCollider2D>().size = Size;
        var orient = (!animator.GetComponent<SpriteRenderer>().flipX) ? -1 : 1;
        projectile.GetComponent<Direction>().Orient = orient;
        projectile.transform.position = new Vector2(animator.transform.position.x + (offSet.x * orient), animator.transform.position.y + offSet.y);

            spawn = false;
        }

        if (!spawn)
        {
            timeElapsed = 0;
        }
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        spawn = true;
    }
}
