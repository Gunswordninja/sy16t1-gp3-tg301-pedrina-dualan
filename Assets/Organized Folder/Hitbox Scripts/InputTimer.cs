﻿using UnityEngine;
using System.Collections;

public class InputTimer : MonoBehaviour {

	public float Setframes;
	private bool timed; 
	public float currentTime;
    public float duration;

	// Use this for initialization
	void Start () {
		Setframes = Setframes / 30;
	}
	
	// Update is called once per frame
	void Update () {
        if(timed)
		    currentTime -= Time.deltaTime;
	}

	public void StartTimer()
	{
		currentTime = duration;
        timed = true;
	}

    public void StopTimer()
    {
        currentTime = duration;
        timed = false;
    }

    public float ShowCurrentTime()
    {
        return currentTime;
    }
}
