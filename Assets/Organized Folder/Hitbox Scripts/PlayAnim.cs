﻿using UnityEngine;
using System.Collections;

public class PlayAnim : MonoBehaviour {

    //script for special moves given they get activated via sequence of multiple presses
    //general idea is to have onject it's attached to name be activator for state that has the same name 
    public bool isAttack;
    private Animator animator;

    void Start()
    {
        animator = GetComponentInParent<Animator>();
    }
   
	void FixedUpdate()
	{
		/*if(animator.GetCurrentAnimatorStateInfo(0).IsName(gameObject.name))
			{
				animator.SetBool("IsAttacking",false);
			}*/
	}

	public void Play(string animName)
    {
        
        if (isAttack)
            animator.SetBool("IsAttacking",true);
        animator.SetTrigger(animName);
        //animator.Play(animName);
    }
}
