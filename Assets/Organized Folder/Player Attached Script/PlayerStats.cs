﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {

    public float WalkSpeed;
	public float DashSpeed;
    public float Health;
	public float JumpHeight;
	public float baseDamage;

}
