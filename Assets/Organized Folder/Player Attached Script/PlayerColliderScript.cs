﻿using UnityEngine;
using System.Collections;

public class PlayerColliderScript : MonoBehaviour {

	// Use this for initialization
	private Animator animator;
	public bool IsInvincible; 
    public Canvas UI;

	void Start () {

		IsInvincible = false;
		animator = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void TakeDamage(float Damage)
	{
		// Hazardbox for enemy hitboxes
		if (!IsInvincible)
		{
			UI.SendMessage("Damage", Damage);
			IsInvincible = true;
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.tag == "PickUp")
		{
			GetComponent<ItemEffects>().GetItem(other.GetComponent<ItemID>().CurrentItemID);
			Destroy(other.gameObject);
		}
	}

}
