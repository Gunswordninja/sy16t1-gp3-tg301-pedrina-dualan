﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Aware of this being a sh*t way but will incorporate to the actual scriptts it should be in the near future
public class Orientation : MonoBehaviour {

    private TempPlayerEssentials tempES;
    private SpriteRenderer sRenderer;

    void Start()
    {
        sRenderer = GetComponent<SpriteRenderer>();
        tempES = GetComponent<TempPlayerEssentials>();
    }

    public string MoveAdjustment(string moveSignature)
    {
        if (moveSignature == "FORWARD" && !sRenderer.flipX)
            return "BACKWARD";
        else if (moveSignature == "BACKWARD" && !sRenderer.flipX)
            return "FORWARD";
        else
            return moveSignature;
    }

    //public float HitboxAdjustment(float x)
    //{
    //    if (tempES.IsFacingLeft)
    //        return x *= -1;
    //    else
    //        return x;
    //}

    //public void SpriteAdjust()
    //{
    //    if (tempES.IsFacingLeft)
    //        sRenderer.flipX = false;
    //    else
    //        sRenderer.flipX = true;
    //}
}
