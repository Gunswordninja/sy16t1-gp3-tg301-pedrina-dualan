﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class InputKey{

    public string name;
    public KeyCode key;
}
