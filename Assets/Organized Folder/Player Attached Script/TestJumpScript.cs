﻿using UnityEngine;
using System.Collections;

public class TestJumpScript : MonoBehaviour {
	private float jumpForce;
	
	// Update is called once per frame
	public void ApplyForce () {

		jumpForce = gameObject.GetComponent<PlayerStats>().JumpHeight;
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
    }
}
