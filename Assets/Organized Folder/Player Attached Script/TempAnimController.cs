﻿using UnityEngine;
using System.Collections;

public class TempAnimController : MonoBehaviour {

    private TempPlayerEssentials tempES;
    private Animator animator;
    private KeyBindings keyBindings;
    private int moveStateHash;

    void Start()
    {
        animator = GetComponent<Animator>();
        keyBindings = GetComponent<KeyBindings>();
        tempES = GetComponent<TempPlayerEssentials>();
    }

    void Update()
    {
        for (int x = 0; x < keyBindings.keys.Count; x++)
        {
            if (keyBindings.keys[x].name == "FORWARD" || keyBindings.keys[x].name == "BACKWARD")
            {
                if (Input.GetKey(keyBindings.keys[x].key))
                {
                    animator.SetBool("IsWalking", true);
                    if (keyBindings.keys[x].name == "FORWARD" && Input.GetKeyDown(keyBindings.keys[x].key))
                        tempES.IsFacingLeft = false;
                    else if (keyBindings.keys[x].name == "BACKWARD" && Input.GetKeyDown(keyBindings.keys[x].key))
                        tempES.IsFacingLeft = true;
                }
                if (Input.GetKeyUp(keyBindings.keys[x].key))
                {    
                    animator.SetBool("IsWalking", false);
                }
            }
            if (keyBindings.keys[x].name == "UP")
            {
                if (Input.GetKeyDown(keyBindings.keys[x].key))
                {
                    if (animator.GetBool("IsGrounded"))
                    {
                        animator.SetTrigger("Jump");
                        animator.SetBool("IsJumping", true);
                        animator.SetBool("IsWalking", false);
                        animator.SetBool("IsGrounded", false);
                    }
                }
            }

            if (keyBindings.keys[x].name == "DOWN")
            {
                if (Input.GetKey(keyBindings.keys[x].key))
                {
                   // animator.SetTrigger("Crouch");
                    animator.SetBool("IsWalking", false);
                    animator.SetBool("IsCrouching", true);     
                }

                if (Input.GetKeyUp(keyBindings.keys[x].key))
                {
                    animator.SetBool("IsCrouching", false);  
                }
            }

			//if Rigidbody2D.
            //if (keyBindings.keys[x].name == "NMEDIUMKICK")
            //{
            //    if (Input.GetKeyUp(keyBindings.keys[x].key))
            //    {
            //        animator.Play("NMediumKick");
            //    }
            //}
        }
    
    }
}
