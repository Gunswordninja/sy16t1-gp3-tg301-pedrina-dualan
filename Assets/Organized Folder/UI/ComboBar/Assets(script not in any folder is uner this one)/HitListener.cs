﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HitListener : MonoBehaviour {

    private InputTimer timer;
    private int hitCount;
    private Text display;
    private Slider slider;
    void OnEnable()
    {
		ComboMeterDamage.onHit += HitCounter;
    }

     void Start()
    { 
        display = GetComponentInChildren<Text>();
        slider = GetComponentInChildren<Slider>();
        timer = GetComponent<InputTimer>();
        slider.maxValue = timer.duration;   
    }

	// Update is called once per frame
	void HitCounter() {
        timer.StartTimer();
        hitCount++;
	}

   
    void Update()
    {
        slider.value = timer.currentTime;
        if (timer.ShowCurrentTime() > 0)
            display.text = "x" + hitCount.ToString()+ "!";
        else
        {
            timer.StopTimer();
            hitCount = 0;
        }
            
    }

    void OnDisable()
    {
        ComboMeterDamage.onHit -= HitCounter;
    }
}
