﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIScript : MonoBehaviour {

    
	public GameObject Player;
    private float CurHP;
	private float MaxHP;
    public float Stocks;
    public Image StockImage;
    public Image HealthBar;
    RectTransform StockPlacementTransform;


	// Use this for initialization
	void Start () {
		MaxHP = Player.GetComponent<PlayerStats>().Health;
        CurHP = MaxHP;
        Stocks = 3;

        for (int i = 1; i <= 3; i++)
        {
            StockImage.transform.localPosition = new Vector2(65 * i, 400);
            StockImage.rectTransform.anchorMin = new Vector2(0, 1);
            StockImage.rectTransform.anchorMax = new Vector2(0, 1);
            StockImage.rectTransform.pivot = new Vector2(.5f, .5f);
            StockImage.name = "Life" + i;
            Instantiate(StockImage, gameObject.transform);
        }

        HealthBar.transform.localPosition = new Vector2(50, 450);
        HealthBar.rectTransform.anchorMin = new Vector2(0, 1);
        HealthBar.rectTransform.anchorMax = new Vector2(0, 1);
        HealthBar.rectTransform.pivot = new Vector2(0f, 0);
        Instantiate(HealthBar, gameObject.transform);

    }
	
	// Update is called once per frame

    void Damage(float damage)
    {
        CurHP -= damage;
        BroadcastMessage("ReduceBar", damage/MaxHP);
        if (CurHP <= 0)
        {
            Death();
        }
    }

	void Heal(float HealAmount)
	{
		CurHP += HealAmount;
		BroadcastMessage("AddBar", HealAmount/MaxHP);
		if (CurHP >= MaxHP)
		{
			CurHP = MaxHP;
			BroadcastMessage("ResetBar");
		}

	}

    void Death()
    {
        Stocks -= 1;
        GameObject ChildtoDelete = gameObject.transform.GetChild((int)Stocks).gameObject;
        Destroy(ChildtoDelete);
		Player.GetComponent<Animator>().Play("Dead State");
        CurHP = MaxHP;
		BroadcastMessage("ResetBar");
        if (Stocks > 0)
        {
            Debug.Log("GameOver boyi");
        }
    }
}
