﻿using UnityEngine;
using System.Collections;

public class Reduce : MonoBehaviour {

	private Vector3 origMeasurement;

	void Start()
	{
		origMeasurement = gameObject.transform.localScale;
	}

	public void ReduceBar(float percentile)
    {
        gameObject.transform.localScale -= new Vector3 (percentile,0,0);
    }

    public void AddBar(float percentile)
    {
        gameObject.transform.localScale += new Vector3(percentile, 0, 0);
    }

	public void ResetBar()
	{
		gameObject.transform.localScale = origMeasurement;
	}
}
