﻿using UnityEngine;
using System.Collections;

public class StateBahviorManager : MonoBehaviour {

    private Animator animator;
    private Rigidbody2D rBody;

    void Start()
    {
        animator = GetComponent<Animator>();
        rBody = GetComponent<Rigidbody2D>();
    }


	// Update is called once per frame
	void Update () {
        if (rBody.velocity == Vector2.zero)
        {
            animator.SetBool("IsGrounded", true);  
        }
        else
            animator.SetBool("IsGrounded", false);

    }
}
