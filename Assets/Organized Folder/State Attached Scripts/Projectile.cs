﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    // Use this for initialization
    public int speed;
    public int orientation;
    // Update is called once per frame

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Enemy")
            Destroy(gameObject);
    }
    void FixedUpdate () {

        transform.Translate(new Vector2(speed * orientation * Time.deltaTime, 0));
      
	}
}
