﻿using UnityEngine;
using System.Collections;

public class Jump : StateMachineBehaviour {

    public float jumpForce;

    // Update is called once per frame
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

		if(animator.GetComponent<Rigidbody2D>().velocity.y <= 1)
		{
			animator.SetBool("IsFalling", true);
			Debug.Log("Y");
		}
    }


	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		//Debug.Log("works");
		animator.SetBool("IsFalling", false);
        jumpForce = animator.GetComponent<PlayerStats>().JumpHeight;
        animator.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        //animator.GetComponent<TestJumpScript>().Invoke("ApplyForce", 0);
	}
	//public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	//{
	//	Debug.Log("ground");
	//	animator.SetBool("IsGrounded", true);
	//}
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
