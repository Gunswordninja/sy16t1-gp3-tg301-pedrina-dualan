﻿using UnityEngine;
using System.Collections;

public class FireballSpawn : StateMachineBehaviour {

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public Vector2 size;
    public Vector2 offSet;

    public GameObject prefab;
    public float SpawnTime;

    private float timeElapsed;
    private bool spawn= false;

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timeElapsed += Time.deltaTime;

        if (timeElapsed >= SpawnTime && spawn) {
            GameObject projectile = Instantiate(prefab) as GameObject;
            //print(animator.name);
            Physics2D.IgnoreCollision(projectile.GetComponent<Collider2D>(), animator.GetComponent<Collider2D>());
            projectile.GetComponent<BoxCollider2D>().size = size;
            var orient = (!animator.GetComponent<SpriteRenderer>().flipX) ? -1 : 1;
            projectile.GetComponent<SpriteRenderer>().flipX = animator.GetComponent<SpriteRenderer>().flipX;
            projectile.GetComponent<Projectile>().orientation = orient;
            projectile.transform.position = new Vector2(animator.transform.position.x + (offSet.x * orient), animator.transform.position.y + offSet.y);
            spawn = false;
        }

        if (!spawn)
        {
            timeElapsed = 0;
        }
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        spawn = true;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
