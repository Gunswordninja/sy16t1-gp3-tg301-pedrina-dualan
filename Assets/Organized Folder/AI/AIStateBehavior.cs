﻿using UnityEngine;
using System.Collections;

public class AIStateBehavior : MonoBehaviour {

    public GameObject target;
    public float speed;
    public float attackRange;
    public bool detect;
    public float waitTime;
    private Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
        if (GetComponentInChildren<Detection>())
            print("Missing detection component in " + gameObject.name);
    }

	// Update is called once per frame
	void Update () {
        //print(target.position);
        if (detect && target != null) //Alert state
        {
            animator.SetBool("IsPatrolling", false);
            animator.SetBool("IsAlerted", true);
            if (Vector2.Distance(transform.position, target.transform.position) <= attackRange)
                animator.SetBool("IsWithinRange", true);
            else
                animator.SetBool("IsWithinRange", false);
        }
        else
        {
            animator.SetBool("IsAlerted", false);
            animator.SetBool("IsPatrolling", true);
        }
    }
}
