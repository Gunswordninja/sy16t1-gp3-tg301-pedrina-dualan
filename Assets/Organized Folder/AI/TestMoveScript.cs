﻿using UnityEngine;
using System.Collections;

public class TestMoveScript : MonoBehaviour {

    public Transform target;
    public float speed;

	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.MoveTowards(transform.position, target.transform.position,Time.deltaTime * speed));
	}
}
