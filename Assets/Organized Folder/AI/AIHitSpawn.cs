﻿using UnityEngine;
using System.Collections;

public class AIHitSpawn : StateMachineBehaviour {
    public Vector2 offSet;
    public Vector2 size;
    public GameObject hurtbox;
    private AIOrientation orient;
    public float delay;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        orient = animator.GetComponent<AIOrientation>();
        if (hurtbox) {

            animator.GetComponent<AIStateBehavior>().StartCoroutine(Delay(animator));
        }
        //box.transform.position = offSet 
    }

    IEnumerator Delay(Animator animator)
    {
        yield return new WaitForSeconds(delay);
        var box = Instantiate(hurtbox) as GameObject;
        Physics2D.IgnoreCollision(animator.GetComponent<Collider2D>(), box.GetComponent<Collider2D>());
        int x = (orient.CheckOrientation()) ? 1 : -1;
        Debug.Log(offSet);

        box.GetComponent<BoxCollider2D>().size = size;
        box.transform.position = (Vector2)animator.transform.position + (offSet * x);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
