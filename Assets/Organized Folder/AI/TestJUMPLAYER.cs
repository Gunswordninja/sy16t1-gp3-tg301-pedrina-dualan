﻿using UnityEngine;
using System.Collections;

public class TestJUMPLAYER : MonoBehaviour {

	void Update () {
        if (Input.GetKeyDown(KeyCode.W))
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 12, ForceMode2D.Impulse);
            GetComponent<Animator>().SetTrigger("Jump");
        }
	}
}
