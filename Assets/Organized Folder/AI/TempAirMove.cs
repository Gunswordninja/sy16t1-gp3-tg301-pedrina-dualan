﻿using UnityEngine;
using System.Collections;
using System.Linq;


public class TempAirMove : StateMachineBehaviour {
    private KeyBindings keyBindings;
    private float speed;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        keyBindings = animator.GetComponent<KeyBindings>();
        speed = animator.GetComponent<PlayerStats>().WalkSpeed;
    }


    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Input.GetKey(KeyCode.A))
            animator.transform.position = Vector2.MoveTowards(animator.transform.position, new Vector2(animator.transform.position.x - 6, animator.transform.position.y), Time.deltaTime * speed);
        else if (Input.GetKey(KeyCode.D))
            animator.transform.position = Vector2.MoveTowards(animator.transform.position, new Vector2(animator.transform.position.x + 6, animator.transform.position.y),Time.deltaTime * speed);

    }
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
