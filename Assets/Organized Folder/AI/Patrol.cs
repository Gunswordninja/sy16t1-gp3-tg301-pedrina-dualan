﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Patrol : StateMachineBehaviour {
    public float patrolRange;
    private List<Vector2> targets;
    private float posX;
    private float posY;
    public bool halfway = true;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        targets = new List<Vector2>();
        posX = animator.transform.position.x;
        posY = animator.transform.position.y;
        Debug.Log(animator.transform.position);

        targets.Add(new Vector2(posX + patrolRange, posY));
        targets.Add(new Vector2(posX - patrolRange, posY));
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Vector2.Distance(animator.transform.position, targets[Convert.ToInt32(halfway)]) < 0.5f)
        {
            if (halfway) halfway = false;
            else halfway = true; 
        }
        StartPatrol(animator);
    }

    void StartPatrol(Animator animator)
    {
        //animator.transform.position = Vector2.MoveTowards(animator.transform.position,);
        //foreach (Vector2 pos in targets)
        if (halfway)
            animator.GetComponent<SpriteRenderer>().flipX = true;
        else
            animator.GetComponent<SpriteRenderer>().flipX = false;
        animator.transform.position = Vector2.MoveTowards(animator.transform.position, targets[Convert.ToInt32(halfway)], Time.deltaTime * animator.GetComponent<Stats>().speed);
    }


    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
