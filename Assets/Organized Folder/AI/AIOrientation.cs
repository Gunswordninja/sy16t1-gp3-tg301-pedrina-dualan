﻿using UnityEngine;
using System.Collections;

public class AIOrientation : MonoBehaviour {
    private AIStateBehavior behavior;
    private SpriteRenderer sRenderer;
	// Use this for initialization
	void Start () {
        behavior = GetComponent<AIStateBehavior>();
        sRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (behavior.target)
            SetOrientation();
    }

    void SetOrientation() {
        if (CheckOrientation()) sRenderer.flipX = true;
        else sRenderer.flipX = false;
    }

    public bool CheckOrientation()
    {
        if  (behavior.target.transform.position.x - transform.position.x < 0)
            return true;
        else
            return false;
    }
}
