﻿using UnityEngine;
using System.Collections;

public class Detection : MonoBehaviour {

    private AIStateBehavior ai;
    public string targetTag;
    void Start()
    {
        ai = GetComponentInParent<AIStateBehavior>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == targetTag) {
            ai.detect = true;
            //Debug.Log(col.transform.gameObject.transform.position);
            ai.target = col.transform.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == targetTag) {
            ai.detect = false;
            ai.target = null;
        }
    }



}
