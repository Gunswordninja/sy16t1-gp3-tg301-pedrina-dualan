﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class SpawnRandomItem : MonoBehaviour {

	public List<GameObject> ItemDrops = new List<GameObject>();

	// Use this for initialization
	void OnDestroy()
	{
		Instantiate(ItemDrops[Random.Range(0,ItemDrops.Count-1)],gameObject.transform.position,gameObject.transform.rotation);
	}
}
