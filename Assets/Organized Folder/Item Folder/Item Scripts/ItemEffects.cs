﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemEffects : MonoBehaviour {

	private float modifiedDamage;
	private float modifiedJump;
	private float modifiedSpeed;
	private float baseDamage;
	private float baseJump;
	private float baseSpeed;
	public Canvas ItemUi;
	

	// Use this for initialization
	void Start () {

        baseDamage = GetComponent<PlayerStats>().baseDamage;
		baseJump = GetComponent<PlayerStats>().JumpHeight;
		baseSpeed = GetComponent<PlayerStats>().baseDamage;

        modifiedDamage = baseDamage + 5;
		modifiedJump = baseJump + 5;
		modifiedSpeed = baseSpeed + 5;
        //total = base + EventModifiers;
    }

	void Normalize()
	{
		StopCoroutine("BuffTimer");
		GetComponent<PlayerStats>().baseDamage = baseDamage;
		GetComponent<PlayerStats>().JumpHeight = baseJump;
		GetComponent<PlayerStats>().WalkSpeed = baseSpeed;
	}

    // Update is called once per frame
    public void GetItem(int ItemId)
	{
		switch(ItemId)
		{
		case 1 :  
			Normalize();
			GetComponent<PlayerStats>().baseDamage = modifiedDamage;
			StartCoroutine("BuffTimer",5);
			break;

		case 2 :  
			Normalize();
			GetComponent<PlayerStats>().JumpHeight = modifiedJump;
			StartCoroutine("BuffTimer",5);
			break;

		case 3 :  
			Normalize();
			GetComponent<PlayerStats>().WalkSpeed = modifiedSpeed;
			StartCoroutine("BuffTimer",5);
			break;

		case 4 : ItemUi.SendMessage("Heal",25); break;
		
		case 5 : ItemUi.SendMessage("Heal",50); break;

		case 6 : ItemUi.SendMessage("Heal",100); break;

		}
	}

    IEnumerator BuffTimer(float seconds)
    {
        yield return  new WaitForSeconds(seconds);
		Normalize();
		StopCoroutine("BuffTimer");
    }	
}
