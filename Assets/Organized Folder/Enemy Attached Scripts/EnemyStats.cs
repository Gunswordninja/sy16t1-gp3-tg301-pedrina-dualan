﻿using UnityEngine;
using System.Collections;

public class EnemyStats : MonoBehaviour {

    public int hp;
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (hp <= 0)
		{
			animator.Play("Death");
		}
    }
}
