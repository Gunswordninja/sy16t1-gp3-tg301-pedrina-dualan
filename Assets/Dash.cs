﻿using UnityEngine;
using System.Collections;

public class Dash : StateMachineBehaviour {

    private float dashSpeed;
    private PlayerStats stats;
    private TempPlayerEssentials tempES;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        stats = animator.GetComponent<PlayerStats>();
        tempES = animator.GetComponent<TempPlayerEssentials>();

        dashSpeed = stats.DashSpeed;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetComponent<TempPlayerEssentials>().IsFacingLeft)
        {
            animator.GetComponent<SpriteRenderer>().flipX = false;
            animator.transform.Translate(Vector3.left * dashSpeed * Time.deltaTime);
        }
        if (!animator.GetComponent<TempPlayerEssentials>().IsFacingLeft)
        {
            animator.GetComponent<SpriteRenderer>().flipX = true;
            animator.transform.Translate(Vector3.right * dashSpeed * Time.deltaTime);
        }
    }


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
