﻿using UnityEngine;
using System.Collections;

public class AerialExpire : MonoBehaviour {

    private Animator animator;

    void Start()
    {
        animator = GetComponentInParent<Animator>();
    }

	// Update is called once per frame
	void Update () {
        if (animator.GetBool("IsGrounded"))
            Destroy(gameObject);
	}
}
